export function logarTempoDeExecucao(emSegundos: boolean = false) {

    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor){

        const metodoOriginal = descriptor.value;

        descriptor.value = function(...args: any[]){

            let divisor = 1;
            let unidade = 'ms';
            if (emSegundos){
                divisor = 1000;
                unidade = 's';
            }

            console.log(`-----------------------`);
            console.log(`Parâmetros do método ${propertyKey}: ${JSON.stringify(args)} `);
                    
            const t1 = performance.now();
            
            const retorno = metodoOriginal.apply(this,args);

            const t2 = performance.now();

            console.log(`Retorno do método ${propertyKey}: ${JSON.stringify(retorno)}`);
            console.log(`Tempo de execução do método ${propertyKey}: ${(t1 - t2)/divisor} ${unidade}`);

            return retorno;
        }
        return descriptor;
    }
}